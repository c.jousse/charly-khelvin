#ifndef ALBUM_H
#define ALBUM_H
#include <iostream>
#include <chrono>
class Album
{

 private :
  unsigned int id;
  std::chrono::time_point<std::chrono::system_clock> date;
  std::string name;

  unsigned int get_id();
  void set_id(unsigned int);

  void get_date();
  void set_date(std::chrono::time_point<std::chrono::system_clock>);

  std::string get_name();
  std::string set_name(std::string);


 public :
  Album();
  Album(unsigned int, std::chrono::time_point<std::chrono::system_clock>, std::string);




};
#endif
