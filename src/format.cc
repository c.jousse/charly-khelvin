#include "format.h"

#include <iostream>
#include <string>

Format::Format() :
  id(0),
  titule("")
{}

Format::Format(unsigned int _id, std::string _titule) :
  id(_id),
  titule(_titule)
{}

Format::~Format()
{
  std::cout << "Destructeur par défaut" << std::endl;
}

unsigned int Format::get_id()
{
  return id;
}

void Format::set_id(unsigned int _id)
{
  id = _id;
}
std::string Format::get_titule()
{
  return titule;
}

void Format::set_titule(std::string _titule)
{
  titule = _titule;
}
