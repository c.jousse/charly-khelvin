#include <iostream>
#include <string>
#include "album.h"

Album::Album() :
  id(0),
  name("")
{
  std::chrono::system_clock::now();
}

Album::Album(unsigned int _id, std::chrono::time_point<std::chrono::system_clock> _date, std::string _name) :
  id(_id),
  date(_date),
  name(_name)
{}

Album::~Album()
{
  std::cout << "Destructeur par défaut" << std::endl;
}

unsigned int Album::get_id()
{
  return id;
}

void Album::set_id(unsigned int _id)
{
  id = _id;
}
std::string Album::get_name()
{
  return name;
}

std::string Album::set_name(std::string _name)
{
  name = _name;
};



