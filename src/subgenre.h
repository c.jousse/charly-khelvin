#ifndef SUBGENRE_H
#define SUBGENRE_H
#include <string>


class Subgenre
{
 public:


  Subgenre();


  Subgenre(unsigned int _id, std::string _sub_type);


  unsigned int get_id();

  std::string get_sub_type();

 private:
  unsigned int id;
  std::string sub_type;
};
#endif
