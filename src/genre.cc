#include "genre.h"

#include <iostream>
#include <string>

Genre::Genre() :
  id(0),
  type("")
{}

Genre::Genre(unsigned int _id, std::string _type) :
  id(_id),
  type(_type)
{}

Genre::~Genre()
{
  std::cout << "Destructeur par défaut" << std::endl;
}

unsigned int Genre::get_id()
{
  return id;
}

void Genre::set_id(unsigned int _id)
{
  id = _id;
}

std::string Genre::get_type()
{
  return type;
}

void Genre::set_type(std::string _type)
{
  type = _type;
}
